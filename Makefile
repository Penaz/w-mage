CC=g++
CCFLAGS=-Wall -Wextra -Werror -pedantic -march=x86-64

docserver: docs
	python3 -m http.server & xdg-open "http://127.0.0.1:8000/docs/html"

docs: includes/*
	doxygen

clean:
	rm -rf *.o *.so *.a ./docs/*
	echo "Finished Cleaning"

