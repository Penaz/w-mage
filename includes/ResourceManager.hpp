/*
 * --------------------------------------------------
 * Resource Manager Header file
 * --------------------------------------------------
 * This is the header file for the ResourceManager class,
 * a map-based container used to store and retrieve shared resources quickly
 */

#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include <unordered_map>
#include <memory>
#include <SFML/System.hpp>

/**
 * \class ResourceManager
 * \brief An associative container for Shared Resources
 * \details The ResourceManager class implements an associative container for
 *         SFML Textures, with load, delete, update and clear capabilities.
 *         This class is intended to be lightweight and fast.
 *         This class includes a mutex for unsafe operations on its container.
 * \tparam ID The type used as key
 * \tparam Resource The type used as resource to load
 */
template<typename ID, class Resource>
class ResourceManager{
    /*
     * --------------------
     * Private Fields
     * --------------------
     */
    private:
        std::unordered_map<ID, std::unique_ptr<Resource>> container;
        /* A mutex for threaded access */
        sf::Mutex mutex;
    /*
     * --------------------
     * Public Methods
     * --------------------
     */
    public:
        /**
         * \brief The constructor of the container
         * \details This method prepares the container and initializes
         *          the class for usage
         */
        ResourceManager();
        /**
         * \brief The container destructor
         * \details This method is called upon destruction of the container,
         *          freeing memory
         */
        ~ResourceManager() = default;
        /**
         * \brief Getter function for a shared resource
         * \details This method gets a Resource from the container, given
         *          the key.
         *
         * \exception std::out_of_range Thrown in case the key isn't in the container
         *
         * \param key The key to identify the Resource
         * \return A unique pointer to the Resource
         */
        Resource& get(const ID& key);
        /**
         * \brief Remove method for a Resource
         * \details This method removes a Resource from the container, given
         *          the key.
         *
         * \param key The key used to identify the Resource
         */
        bool remove(const ID& key);
        /**
         * \brief Adds a new resource to the container
         * \details This method adds a new resource to the container,
         *          linking it to the given key.
         *          In case of duplicated key, the old Resource will be
         *          overwritten.
         *
         * \param path The path used to load the Resource
         * \param key The key used to identify the Resource
         * \param par The second parameter to pass to LoadFromFile
         * \tparam Param The additional parameter to be passed to loadfromfile
         *
         * \return A boolean: true if the insertion is successful, false
         *                    otherwise
         */
        template<typename Param>
        bool add(const std::string& path, const ID& key, const Param& par);
        /**
         * \brief Adds a new resource to the container
         * \details This method adds a new resource to the container,
         *          linking it to the given key.
         *          In case of duplicated key, the old Resource will be
         *          overwritten.
         *
         * \param path The path used to load the Resource
         * \param key The key used to identify the Resource
         *
         * \return A boolean: true if the insertion is successful, false
         *                    otherwise
         */
        bool add(const std::string& path, const ID& key);
        /** \brief Clears the container
         * \details This method empties the container, clearing all data
         *          saved.
         */
        void clear();
};

#include "../src/ResourceManager.inl"

#endif /* end of include guard: RESOURCEMANAGER_H */

// vi: cc=80
