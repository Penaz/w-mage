#ifndef APPLICATION_H
#define APPLICATION_H

#include <SFML/Graphics.hpp>
#include "states/StateManager.hpp"

/** \brief The Main Application class
 * \details This class represents the main application, with its game loop
 *          and basic fields
 */
class Application{
    public:
        /** \brief Constructor
         * \details Creates the RenderWindow, State Stack and Clock,
         *          prepares the application for execution.
         */
        Application();
        /** \brief Register the State Factories
         * \details This is a method used to register all the state factories
         *          for later usage. It does not build any state
         */
        void registerStates();
        /** \brief Processes window events
         * \details This method processes all events in the window, and
         *          sends them to the underlying objects in the state stack
         */
        void processEvents();
        /** \brief Updates the game state
         * \details This method sends the updates to the underlying
         *          state stack
         */
        void update(sf::Time dt);
        /** \brief Starts the game loop
         * \details This method starts the game loop, the application will
         *          automatically stop when the state stack is emptied
         */
        void run();
        /** \brief Renders the window
         * \details This method takes care of clearing the screen and drawing
         *          the content of the state stack
         */
        void render();
    private:
        /** \brief The Render window instance */
        sf::RenderWindow mWindow;
        /** \brief The Game Clock */
        sf::Clock mClock;
        /** \brief The Game State Stack */
        StateManager mStack;
        /** \brief The Texture Manager */
        TextureManager mTextures;
        /** \brief The Font Manager */
        FontManager mFonts;
        /** \brief The seconds per frame */
        static sf::Time TimePerFrame;
};

#endif /* end of include guard: APPLICATION_H */
