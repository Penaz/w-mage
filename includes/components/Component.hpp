#ifndef COMPONENT_H
#define COMPONENT_H

/**
 * \class Component
 * \brief A Component that encapsulates a behaviour
 * \details The Component class is used as an interface that defines and
 *          encapsulates a certain behaviour, avoiding oversized classes
 */
class Component{
    public:
        /**
         * \brief The component constructor
         * \details A placeholder for the Component's constructor
         */
        Component();

        /**
         * \brief Updates the state of the component
         * \details This method updates the internal state of the component
         *
         * \param dt The Time delta, representing the time passed since the
         *           previous frame
         */
        virtual void Update(float dt) = 0;
};

#endif /* end of include guard: COMPONENT_H */
