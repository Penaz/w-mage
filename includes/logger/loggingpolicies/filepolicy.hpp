#ifndef FILEPOLICY
#define FILEPOLICY
#include "../LoggingPolicy.hpp"
#include <fstream>
#include <mutex>
/** \brief File Output Policy
 * \details This policy is used for logging into a file
 */

class FilePolicy: public LoggingPolicy{
    private:
        std::mutex outputmutex;
        std::ofstream fileStream;
    public:
        /** Class defining the modes a file is opened */
        enum class OpenMode{
            APPEND,
            OVERWRITE,
        };
        /** \brief Write to the policy's output */
        virtual void write(const std::string& msg) override;
        /** \brief Destructor
         * \details This destructor should finalize the logging sink (flushing buffers, etc...)
         */
        virtual ~FilePolicy();
        /** \brief Constructor
         * \details Constructs the policy and opens, appends or overwrites the logfile
         *
         * \param path - The path to the file
         * \param mode - The mode used to open the file
         */
        FilePolicy(const std::string& path, OpenMode mode = OpenMode::APPEND);

};
#endif /* end of include guard: FILEPOLICY */
