#ifndef STDOUTPOLICY
#define STDOUTPOLICY
#include "../LoggingPolicy.hpp"
#include <iostream>
#include <mutex>
/** \brief Standard Output Policy
 * \details This policy is used for testing, outputs the log contents into the standrd output
 */
class StdOutPolicy: public LoggingPolicy{
    private:
        std::mutex outputmutex;
    public:
        /** \brief Write to the policy's output */
        virtual void write(const std::string& msg) override;
        /** \brief Destructor
         * \details This destructor should finalize the logging sink (flushing buffers, etc...)
         */
        virtual ~StdOutPolicy() = default;
};
#endif /* end of include guard: STDOUTPOLICY */
