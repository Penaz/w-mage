#ifndef LOGGINGPOLICY_H
#define LOGGINGPOLICY_H
#include <string>
/** \brief Logging Policy Interface
 * \details This class defines an interface for a logging policy
 */
class LoggingPolicy{
    public:
        /** \brief Write to the policy's output */
        virtual void write(const std::string& msg) = 0;
        /** \brief Destructor
         * \details This destructor should finalize the logging sink (flushing buffers, etc...)
         */
        virtual ~LoggingPolicy() = default;
};
#endif /* end of include guard: LOGGINGPOLICY_H */
