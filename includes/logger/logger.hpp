#ifndef LOGGER_H
#define LOGGER_H
#include <unordered_map>
#include <string>
#include "LoggingPolicy.hpp"
#include <stdexcept>
#include <ctime>
/** \brief Class for logging
 * \details This class is used for logging purposes.
 */
class Logger{
    public:
        /** Enumerator with the logging levels */
        enum class LoggingLevel: short{
            CRITICAL = 4,
            ERROR    = 3,
            WARNING  = 2,
            INFO     = 1,
            DEBUG    = 0
        };
        /** \brief Sets the output policy for this logger
         */
        void setPolicy(LoggingPolicy* lp);
        /** Sets the logger verbosity */
        void setVerbosity(LoggingLevel lvl);
        /** \brief Gets the logger with the chosen id, creating one
         * if necessary, with a default logging level of INFO
         */
        static Logger* getLogger(const std::string& id, LoggingPolicy* lp = nullptr);
        /** \brief Destructor
         * \details This destructor should perform some finalizing operations (writing end of log messages, flushing buffers...)
         */
        virtual ~Logger();
    public:
        /** \brief Writes an "INFO" level log entry */
        void info(const std::string& msg);
        /** \brief Writes a "DEBUG" level log entry */
        void debug(const std::string& msg);
        /** \brief Writes an "ERROR" level log entry */
        void error(const std::string& msg);
        /** \brief Writes a "WARNING" level log entry */
        void warning(const std::string& msg);
        /** \brief Writes a "CRITICAL" level log entry */
        void critical(const std::string& msg);
    private:
        /** \brief Contains the list of active loggers */
        static std::unordered_map<std::string, Logger*> active_loggers;
    private:
        /** The logger's output policy */
        LoggingPolicy* policy = nullptr;
        /** The logger verbosity */
        LoggingLevel level = LoggingLevel::INFO;
    private:
        /** \brief Private Constructor
         * \details The constructor is made private to allow for a singleton
         * pattern, this way a single instance (or multiple instances) of the
         * logger can be used through the code
         *
         * \param lp The Logging Policy to tie to the logger
         *
         */
        Logger(LoggingPolicy* lp);
        /** \brief Writes a message on the log, using the LoggingPolicy
         * functions
         */
        void message(const std::string& msg);
        /** \brief Logger should be non-copiable */
        Logger(const Logger& other) = delete;
};
#endif /* end of include guard: LOGGER_H */
