/** \brief Type definitions for quicker access */
#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "ResourceManager.hpp"

typedef ResourceManager<std::string, sf::Texture> TextureManager;
typedef ResourceManager<std::string, sf::SoundBuffer> SoundBufferManager;
typedef ResourceManager<std::string, sf::Shader> ShaderManager;
typedef ResourceManager<std::string, sf::Font> FontManager;

#endif /* end of include guard: TYPEDEFS_H */
