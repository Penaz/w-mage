/** \brief Namespace containing enumerators for States
 * \details This namespace contains enumerators used to identify states in
 *          the state stack, managed by the StateManager
 */
#ifndef STATEIDENTIFIERS_H
#define STATEIDENTIFIERS_H

namespace States{
    /** \brief State Identifiers
     * \details IDs used to identify the states in the State Manager
     */
    enum class ID{
        Title,    // Title Screen (Press Key to Continue)
        MainMenu, // Main Menu
        Settings, // Settings Screen
        Game,     // The Game
        Pause,    // Pause Screen
    };
}

#endif /* end of include guard: STATEIDENTIFIERS_H */
