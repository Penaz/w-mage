#ifndef STATE_H
#define STATE_H
#include <memory>
#include <SFML/Window.hpp>
#include "StateIdentifiers.hpp"
#include "../Typedefs.hpp"

/** \brief An Application State
 * \details This class represents a state that can be defined,
 *          such state can represent the main game, a menu or something else
 */

class StateManager;  // Forward Declaration

class State{
    public:
        /** \brief A quicker way to define a unique_ptr, to save on typing */
        typedef std::unique_ptr<State> statePtr;
        /** \brief The application/state context
         * \details This context contains references to all useful pointers,
         *          like Resource Managers, Window and such.
         */
        struct Context{
            public:
                Context(sf::RenderWindow& win, TextureManager& textures, FontManager& fonts);
                sf::RenderWindow* window;
                TextureManager* textureManager;
                FontManager* fontManager;
        };

    public:
        /** \brief Constructor
         * \param stack Pointer to the State Manager/Stack
         * \param context Context of the state/application
         */
        State(StateManager& stack, Context context);
        /** \brief Destructor */
        virtual ~State() = default;

        /** \brief Draws the state on screen
         * \details Method used to abstract the drawing routine of a state,
         *          draws the state on screen.
         */
        virtual void draw() = 0;
        /** \brief Updates the state
         * \details Method used to abstract the updating of the state.
         * \param dt The time elapsed between the previous and the current
         *           frame
         * \returns A boolean defining if the following states in the stack
         *          should be updated
         */
        virtual bool update(sf::Time dt) = 0;
        /** \brief Handles events
         * \details Method used to handle incoming window events
         * \param event The incoming window event
         * \returns A boolean defining if the following states in the stack
         *          should be updated
         */
        virtual bool handleEvent(const sf::Event& event) = 0;

    protected:
        /* \brief Request a push to the state stack
         * \details Requests the state manager to push a new state in its
         *          state stack. The push is not performed immediately, but
         *          enqueued to be performed at the end of the frame update.
         * \param stateID The ID of the state to push
         */
        void requestStackPush(States::ID stateID);
        /* \brief Request a pop from the state stack
         * \details Requests the state manager to pop a state from its
         *          state stack. The pop is not performed immediately, but
         *          enqueued to be performed at the end of the frame update.
         *          WARNING: If the stack is left empty, the application will close
         */
        void requestStackPop();
        /* \brief Request a clear of the state stack
         * \details Requests the state manager to clear the
         *          state stack. The clear is not performed immediately, but
         *          enqueued to be performed at the end of the frame update.
         *          WARNING: If the stack is left empty, the application will close
         */
        void requestStackClear();

        /* \brief Gets the state context
         * \details Returns the context structure of the state
         * \returns The state context
         */
        Context getContext() const;

    private:
        /* \brief Pointer to the state manager/state stack */
        StateManager* mStack;
        /* \brief The state context */
        Context mContext;
};

#endif /* end of include guard: STATE_H */
