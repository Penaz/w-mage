#ifndef STATEMANAGER_H
#define STATEMANAGER_H

#include <SFML/System.hpp>
#include "State.hpp"
#include "StateIdentifiers.hpp"
#include <map>
#include <list>
#include <functional>
/** \brief A non-copyable Manager for application states
 * \details This class is a glorified stack for states, used to store, retrieve
 *          and manage game states
 */
class StateManager : private sf::NonCopyable{
    public:
        enum class Action{
            Push,
            Pop,
            Clear
        };
    public:
        /** \brief Constructor
         * \details Constructs the State Manager, given the application context
         * \param context The application context
         */
        explicit StateManager(State::Context context);

        /** \brief Registers a new State Factory in the Manager
         * \details Registers a new factory to create a new state, given the
         *          state ID and the template parameter
         * \tparam T The derived state class to register the factory for
         * \param stateID The State ID to register the factory for
         */
        template <typename T>
        void registerStateFactory(States::ID stateID){
            mFactories[stateID] = [this]() {
                return State::statePtr(new T(*this, mContext));
            };
        }

        /** \brief Update all updateable states in the state stack
         * \details Updates in order all states in the state stack, checking
         *          if each state allows the next one to update
         * \param dt The time elapsed since last frame was updated
         */
        void update(sf::Time dt);
        /** \brief Draws all states in the state stack
         * \details Draws in reverse order (oldest to newest) all the states in
         *          the state stack.
         */
        void draw();
        /** \brief Handles windows events in the stack
         * \details Handles the events in the state stack, checking if
         *          each state allows the next one to process the event
         * \param event The window event to process
         */
        void handleEvent(const sf::Event& event);
        /** \brief Pushes a state in the stack
         * \details Pushes the given state on top of the state stack
         * \param stateID The ID of the state to push
         */
        void pushState(States::ID stateID);
        /** \brief Removes a state from the stack
         * \details Removes the top state from the state stack
         */
        void popState();
        /** \brief Clears the state stack
         * \details Clears the state stack, removing all elements inside it
         *          WARNING: If the stack stays empty, the application will
         *                   close
         */
        void clearStates();

        /** \brief Checks if the stack is empty
         * \details Returns true if the state stack is empty
         */
        bool isEmpty() const;

    private:
        /** \brief Creates a state, given the State ID
         * \details Creates a new state, using the factory available
         * \param stateID The ID of the state to create
         * \returns A unique pointer to the created state
         */
        State::statePtr createState(States::ID stateID);
        /** \brief Applies all pending changes inside the state stack
         * \details Applies all pending changes at the end of an update cycle
         */
        void applyPendingChanges();

    private:
        /** \brief Structure defining a pending change */
        struct PendingChange{
            // TODO: Fill
            Action action;
            States::ID stateID;
        };

    private:
        /** \brief The real state stack */
        std::list<State::statePtr> mStack;
        /** \brief The pending operations list */
        std::list<PendingChange> mPendingList;
        /** \brief The application context */
        State::Context mContext;
        /** \brief The State factories map */
        std::map<States::ID, std::function<State::statePtr()>> mFactories;
};

#endif /* end of include guard: STATEMANAGER_H */
