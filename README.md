# W-MAGE

W-MAGE (We made another game engine)

W-MAGE is intended to be a well-written (hopefully) and well-documented (hopefully!) no-nonsense collection of containers, components and functionalities for developing games using the SFML libraries.

## Dependencies

- SFML
- A C++11-compatible compiler

For generating documentation:

- Doxygen
- PlantUML

For `make docserver`:

- Python3

## Docs

You can generate documentation using doxygen, with one of the following commands:

- `doxygen`
- `make docs`

You can explore the documentation with the `make docserver` command, which will start a local Python3 server and open a browser pointing to the documentation (via xdg-open).

You can check the UML diagram of the project in `docs/UML`

## Usage

Copy the contents from the `./includes` and `./src` folders into your project to build in the templates provided.

## Functionalities provided

- A Shared Resources Container/Manager
- A (very) basic logging class

## TODOs

- Component-based entities
- Base components
