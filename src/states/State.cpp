#include "../../includes/states/State.hpp"
#include "../../includes/states/StateManager.hpp"

State::Context::Context(sf::RenderWindow& win, TextureManager& textures, FontManager& fonts):
    window(&win),
    textureManager(&textures),
    fontManager(&fonts){}

State::State(StateManager& stack, State::Context context): mStack(&stack), mContext(context){}

void State::requestStackPush(States::ID stateID){
    mStack->pushState(stateID);
}

void State::requestStackPop(){
    mStack->popState();
}

void State::requestStackClear(){
    mStack->clearStates();
}

State::Context State::getContext() const{
    return mContext;
}
