#include "../../includes/states/StateManager.hpp"

StateManager::StateManager(State::Context context): mContext(context){}

State::statePtr StateManager::createState(States::ID stateID){
    auto found = mFactories.find(stateID);
    return found->second();
}

void StateManager::handleEvent(const sf::Event& event){
    // Newest state on the "right side" of the list
    for (auto it = mStack.rbegin(); it != mStack.rend(); ++it){
        if (!(*it)->handleEvent(event)){
            return;
        }
    }
    applyPendingChanges();
}

void StateManager::update(sf::Time dt){
    // Newest state on the "right side" of the list
    for (auto it = mStack.rbegin(); it != mStack.rend(); ++it){
        if (!(*it)->update(dt)){
            return;
        }
    }
}

void StateManager::draw(){
    // Newest state on the "right side" of the list
    for (auto it = mStack.begin(); it != mStack.end(); ++it){
        (*it)->draw();
    }
}

void StateManager::applyPendingChanges(){
    for (PendingChange change : mPendingList){
        // XXX: Make better/lighter by using std::function and binding?
        switch (change.action){
            case Action::Push:
                mStack.push_back(createState(change.stateID));
                break;
            case Action::Pop:
                mStack.pop_back();
                break;
            case Action::Clear:
                mStack.clear();
                break;
        }
    }
    mPendingList.clear();
}

void StateManager::pushState(States::ID stateID){
    PendingChange action;
    action.action = StateManager::Action::Push;
    action.stateID = stateID;
    mPendingList.push_back(action);
}

void StateManager::popState(){
    PendingChange action;
    action.action = StateManager::Action::Pop;
    mPendingList.push_back(action);
}


void StateManager::clearStates(){
    PendingChange action;
    action.action = StateManager::Action::Clear;
    mPendingList.push_back(action);
}

bool StateManager::isEmpty() const{
    return mStack.empty();
}
