/*
 * --------------------------------------------------
 * Resource Manager Implementation file
 * --------------------------------------------------
 * This file implements what defined in the ResourceManager Header file,
 * implementing an efficient map-based container used to store and retrieve
 * Resources.
 */

#include "../includes/ResourceManager.hpp"

/* Constructor: Just builds the container */
template<typename ID, class Resource>
ResourceManager<ID, Resource>::ResourceManager(): container(), mutex(){}

/* Add Method, inserts/replaces a key on the map */
template<typename ID, class Resource>
template<typename Param>
bool ResourceManager<ID, Resource>::add(const std::string& path,
                                        const ID& key,
                                        const Param& par){
    /*
     * The assignment operator of unique_ptr should take care of freeing
     * the memory used by the old texture (if it exists)
     */
    bool success = false;
    {
        /* Critical region, using sf::Lock and sf::Mutex to lock the resource */
        sf::Lock lock(mutex);
        container[key] = std::make_unique<Resource>();
        success = container[key]->loadFromFile(path, par); // Status of the load
        if (!success){
            // Let's not leave empty ptrs around
            container.erase(key);
        }
        // Exiting critical region, mutex is unlocked automatically
    }
    return success;
}

template<typename ID, class Resource>
bool ResourceManager<ID, Resource>::add(const std::string& path,
                                        const ID& key){
    /*
     * The assignment operator of unique_ptr should take care of freeing
     * the memory used by the old texture (if it exists)
     */
    bool success = false;
    {
        /* Critical region, using sf::Lock and sf::Mutex to lock the resource */
        sf::Lock lock(mutex);
        container[key] = std::make_unique<Resource>();
        success = container[key]->loadFromFile(path); // Status of the load
        if (!success){
            // Let's not leave empty ptrs around
            container.erase(key);
        }
        // Exiting critical region, mutex is unlocked automatically
    }
    return success;
}

/* Remove method, scans for the element and erases it */
template<typename ID, class Resource>
bool ResourceManager<ID, Resource>::remove(const ID& key){
    int items_deleted = 0;
    // Safe cast size_type -> int for the number of elements deleted
    {
        /* Critical region, locking mutex */
        sf::Lock lock(mutex);
        items_deleted = container.erase(key);
        // Exiting critical region, mutex is unlocked automatically
    }
    // If an item was deleted, return true, else false.
    return items_deleted != 0;
}

template<typename ID, class Resource>
Resource& ResourceManager<ID, Resource>::get(const ID& key){
    // Can throw an out_of_range exception, although if it happens, it's deserved
    sf::Lock lock(mutex);
    // Locking, in case some thread tries to read while a write is happening
    return *container.at(key);
}

template<typename ID, class Resource>
void ResourceManager<ID, Resource>::clear(){
    /* Just clears the container, calling all destructors, this is dangerous
     * so all the code will be locked */
    sf::Lock lock(mutex);
    container.clear();
    // Exiting critical region, mutex is unlocked automatically
}

// vi: cc=80
