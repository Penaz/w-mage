#include "../../../includes/logger/loggingpolicies/stdoutpolicy.hpp"

void StdOutPolicy::write(const std::string& msg){
    // Locking Mutex, entering critical section
    outputmutex.lock();
    // ---------- CRITICAL SECTION ----------
    std::cout << msg << std::endl;
    // -------- END CRITICAL SECTION --------
    // Unlocking Mutex, exiting critical section
    outputmutex.unlock();
}
