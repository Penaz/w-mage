#include "../../../includes/logger/loggingpolicies/filepolicy.hpp"

FilePolicy::FilePolicy(const std::string& path, OpenMode mode){
    if (mode == OpenMode::APPEND){
        fileStream = std::ofstream(path, std::ofstream::app);
    }
    if (mode == OpenMode::OVERWRITE){
        fileStream = std::ofstream(path, std::ofstream::out);
    }
}

void FilePolicy::write(const std::string& msg){
    if (fileStream.is_open()){
        // Locking Mutex, entering critical section
        outputmutex.lock();
        // ---------- CRITICAL SECTION ----------
        fileStream << msg << std::endl;
        fileStream.flush();
        // -------- END CRITICAL SECTION --------
        // Unlocking Mutex, exiting critical section
        outputmutex.unlock();
    }
}

FilePolicy::~FilePolicy(){
    if (fileStream.is_open()){
        // Locking Mutex, entering critical section
        outputmutex.lock();
        // ---------- CRITICAL SECTION ----------
        fileStream.close();
        // -------- END CRITICAL SECTION --------
        // Unlocking Mutex, exiting critical section
        outputmutex.unlock();
    }
}
