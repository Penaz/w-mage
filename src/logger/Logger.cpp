#include "../../includes/logger/logger.hpp"
#include "../../includes/logger/LoggingPolicy.hpp"
#include <iomanip>
#include <sstream>
#include <iostream>

Logger::Logger(LoggingPolicy* lp): policy(lp){
    policy->write("------------------------- Logging Started -------------------------");
}

Logger* Logger::getLogger(const std::string& id, LoggingPolicy* lp){
    // Search if id exists already
    std::unordered_map<std::string, Logger*>::const_iterator item = active_loggers.find(id);
    // If it exists, return the instance
    if (item != active_loggers.end()){
        return item->second;
    }
    // If it doesn't, create a new one and return it, if the logging policy is not empty
    if (lp != nullptr){
        Logger* nlg = new Logger(lp);
        active_loggers[id] = nlg;
        return nlg;
    }else{
        throw std::runtime_error("Tried to initialize a logger without an output policy");
    }
}

void Logger::setVerbosity(LoggingLevel lvl){
    level = lvl;
}

void Logger::setPolicy(LoggingPolicy* lp){
    policy = lp;
}

void Logger::message(const std::string& msg){
    if (policy != nullptr){
        std::time_t time = std::time(0);
        std::tm now = *std::localtime(&time);
        std::ostringstream toSend;
        toSend << "[" << std::put_time(&now, "%d-%m-%Y %H:%M:%S") << "] - " << msg;
        policy->write(toSend.str());
    }else{
        throw std::runtime_error("Tried to use a logger without an output policy");
    }
}

void Logger::info(const std::string& msg){
    if (level <= Logger::LoggingLevel::INFO){
        message("[INFO]:         " + msg);
    }
}

void Logger::debug(const std::string& msg){
    if (level <= Logger::LoggingLevel::DEBUG){
        message("[DEBUG]:        " + msg);
    }
}

void Logger::warning(const std::string& msg){
    if (level <= Logger::LoggingLevel::WARNING){
        message("[WARN]:         " + msg);
    }
}

void Logger::error(const std::string& msg){
    if (level <= Logger::LoggingLevel::ERROR){
        message("[ERROR]:        " + msg);
    }
}

void Logger::critical(const std::string& msg){
    if (level <= Logger::LoggingLevel::CRITICAL){
        message("[!!CRITICAL!!]: " + msg);
    }
}

Logger::~Logger(){
    policy->write("-------------------- End of log --------------------");
}

std::unordered_map<std::string, Logger*> Logger::active_loggers = std::unordered_map<std::string, Logger*>();
