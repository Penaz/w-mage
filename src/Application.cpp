#include "../includes/Application.hpp"

sf::Time Application::TimePerFrame = sf::seconds(1.f/60.f);

Application::Application():
    mWindow(sf::VideoMode(800,600), "Window Title"),
    mTextures(),
    mFonts(),
    mStack(State::Context(mWindow, mTextures, mFonts)){
    registerStates();
    //mStack.pushState(States::ID::FirstState);
}

void Application::registerStates(){
    //TODO: Fill with state registrations, i.e.
    //mStack.registerStateFactory<TitleState>(States::Title)
}

void Application::processEvents(){
    sf::Event event;
    while(mWindow.pollEvent(event)){
        mStack.handleEvent(event);
    }
}

void Application::update(sf::Time dt){
    mStack.update(dt);
}

void Application::render(){
    mWindow.clear();
    mStack.draw();
    mWindow.display();
}

void Application::run(){
    sf::Time timeSinceLastUpdate = sf::Time::Zero;
    while (mWindow.isOpen()){
        sf::Time dt = mClock.restart();
        timeSinceLastUpdate += dt;
        while (timeSinceLastUpdate > TimePerFrame){
            timeSinceLastUpdate -= TimePerFrame;
            processEvents();
            update(TimePerFrame);

            if (mStack.isEmpty()){
                mWindow.close();
            }
        }
        render();
    }
}
